# Указывает Docker использовать официальный образ python 3 с dockerhub в качестве базового образа
FROM python:3.8
# Устанавливает переменную окружения, которая гарантирует, что вывод из python будет отправлен прямо в терминал без предварительной буферизации
ENV PYTHONUNBUFFERED 1
# Устанавливает рабочий каталог контейнера — "app"
WORKDIR /app
COPY requirements.txt /app/requirements.txt
# Копирует все файлы из нашего локального проекта в контейнер
COPY . /app
# Запускает команду pip install для всех библиотек, перечисленных в requirements.txt
RUN pip install -r /app/requirements.txt

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8080"]