from .models import Users
from django.forms import ModelForm, TextInput

class UsersForm(ModelForm):
  class Meta:
    fields =["name","surname","email"]