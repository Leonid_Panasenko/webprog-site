from django.db import models


class Task(models.Model):
  title = models.CharField('Название', max_length=50)
  task = models.TextField('Описание')
  def __str__(self):
    return self.title

class Users(models.Model):
  name = models.CharField('Имя', max_length=50)
  surname = models.CharField('Фамилия', max_length=50)
  email = models.CharField('Почта', max_length=50)
  def __str__(self):
    return self.title