from django.shortcuts import render
from .models import Users
from .forms import UsersForm

def index(request): 
  lang_sel = "rus"
  lang_unsel = "eng"
  date = "__ no date __"
  if "lang" in request.COOKIES:
      lang_sel = request.COOKIES.get("lang")
  if "date" in request.COOKIES:
      date = request.COOKIES.get("date")
    
  return render(request,'main/index.html', {"lang_sel" : lang_sel, "date" : date})
  #else:
    #response = render(request,'main/index.html', {"lang_sel" : lang_sel, "lang_unsel" : lang_unsel})
    #response.set_cookie("index", "aaa")
  #return response

  
def amaranthe(request): 
  return render(request,'main/amaranthe.html')
  
def annisokay(request): 
  return render(request,'main/annisokay.html')

def BMTH(request): 
  return render(request,'main/BMTH.html')
  
def feedback(request): 
  return render(request,'main/feedback.html')  
  
def groups(request): 
  return render(request,'main/groups.html')
  
def login(request): 
  return render(request,'main/login.html')
  
def register(request): 
  return render(request,'main/register.html')  
  
def database(request): 
  form = UsersForm()
  context = {
    'form':form
  }
  return render(request,'main/database.html', context)  