/*var email = document.forms['form']['email'];
var password = document.forms['form']['password'];

var email_error = document.getElementById('email_error');
var pass_error = document.getElementById('pass_error');

email.addEventListener('textInput', email_Verify);
password.addEventListener('textInput', pass_Verify);

function validated() {
    
    if(email.value.length < 9) {
    
        email.style.border = "1px solid red";
        email_error.style.display = "block";
        email.focus();
        return false;
    }
    
    if(password.value.length < 6) {
    
        password.style.border = "1px solid red";
        pass_error.style.display = "block";
        password.focus();
        return false;
    }
}

function email_Verify() {
    
    if(email.value.length >= 8) {
        
        email.style.border = "1px solid silver";
        email_error.style.display = "none";
        return true;
    }
}

function pass_Verify() {
    
    if(password.value.length >= 8) {
        
        password.style.border = "1px solid silver";
        pass_error.style.display = "none";
        return true;
    }
}
*/

////////////////////////////////////////////////////

const form = document.forms["form"];
const button = form.elements["button"];

const inputArr = Array.from(form);
const validInputArr = [];

inputArr.forEach((el) => {
  if(el.hasAttribute("data-reg"))
  {
    el.setAttribute("is-valid", "0");
    validInputArr.push(el);
  }
});

form.addEventListener("input", inputHandler);
button.addEventListener("click", buttonHandler);

function inputHandler({target})
  {
     if(target.hasAttribute("data-reg"))
     {
       inputCheck(target);
     }
  }

function inputCheck(el)
  {
    const inputValue = el.value;
    const inputReg = el.getAttribute("data-reg");
    const reg = new RegExp(inputReg);
    if(reg.test(inputValue))
    {
      el.style.border = "2px solid rgb(0,196,0)";
      el.setAttribute("is-valid", "1");
    }
    else
    {
      el.style.border = "2px solid rgb(255,0,0)";  
      el.setAttribute("is-valid", "0");    
    }
  }

function buttonHandler(e)//el?
  {
     const isAllValid=[];
    
    validInputArr.forEach((el) => {
       isAllValid.push(el.getAttribute("is-valid"));
     });

    console.log(isAllValid);

    const isValid = isAllValid.reduce((acc, current) => 
      {
        return Boolean(Number(acc)) && Boolean(Number(current));
    })
    
    console.log(isValid);

    if(!Boolean(Number(isValid)))
    {
      e.preventDefault();
    }
  }