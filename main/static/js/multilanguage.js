function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

var en = {
      'nav1': 'Main',
      'nav2': 'Main',
      'nav3': 'The authors',
      'nav3_1': 'Daniil',
      'nav3_2': 'Leonid',
      'nav4': 'sign up',
      'nav5': 'log in',
      'nav6': 'Groups',
      'content2': 'Contact us',
      'content3': 'working with databases',
      'content4': 'your attention will provide information about some of musical groups.',
    };
    var ru = {
      'nav1': 'Главная',
      'nav2': 'Главная',
      'nav3': 'Авторы',
      'nav3_1': 'Даниил',
      'nav3_2': 'Леонид',
      'nav4': 'Регистрация',
      'nav5': 'Авторизация',
      'nav6': 'Группы',
      'content2': 'Связаться с нами',
      'content3': 'Работа с бд',
      'content4': 'Вашему вниманию будет представлена информация о нашей группе.',
    };


var p_new_user = document.getElementById("p_new_user");
var p_old_user = document.getElementById("p_old_user");




date = new Date()
if(getCookie("lang")=="en")
{
  changeLang(en);
  document.cookie = "date="+date.toLocaleDateString('en-US');
}
else
{
  changeLang(ru);
  document.cookie = "date="+date.toLocaleDateString('ru-RU');
}


    function changeLang(lan) {
      lengthObj = Object.getOwnPropertyNames(lan).length;
      for (var i = 0; i <= lengthObj - 1; i++) {
        objKey = Object.getOwnPropertyNames(lan)[i];
        document.getElementById(objKey).innerText = func(objKey);
        //console.log(document.getElementById(objKey));
      }
      if(lan == en)
      {
        document.cookie = "lang=en";
        document.cookie = "date="+date.toLocaleDateString('en-US');
      }
      else
      {
        document.cookie = "lang=ru";
        document.cookie = "date="+date.toLocaleDateString('ru-RU');
      }
        
      function func(a) {
        for (key in lan) {
          if (key == a) {
            return (lan[key]);
          }
        }
      }
    }
