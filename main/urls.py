from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('index.html', views.index),
    path('amaranthe.html', views.amaranthe),
    path('annisokay.html', views.annisokay),
    path('BMTH.html', views.BMTH),
    path('feedback.html', views.feedback),
    path('groups.html', views.groups),
    path('login.html', views.login),
    path('register.html', views.register),
    path('database.html', views.database),

]